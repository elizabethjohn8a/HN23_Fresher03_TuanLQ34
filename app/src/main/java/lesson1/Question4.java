package lesson1;

import java.util.Scanner;

public class Question4 {
    public static void main(String[] args) {
        int inputtedMonth = inputMonth();
        figureOut(inputtedMonth);
    }

    private static void figureOut(int inputtedMonth) {
        switch (inputtedMonth){
            case 1:
            case 2:
            case 3:
                System.out.println("Quarter 1!");
                break;
            case 4:
            case 5:
            case 6:
                System.out.println("Quarter 2!");
                break;
            case 7:
            case 8:
            case 9:
                System.out.println("Quarter 3!");
                break;
            case 10:
            case 11:
            case 12:
                System.out.println("Quarter 4!");
                break;
            default:
                System.out.println("Invalid month number!");

        }
    }

    private static int inputMonth() {
        System.out.println("Input a month: ");
        Scanner scanner = new Scanner(System.in);
        int month = scanner.nextInt();
        return month;
    }
}
