package lesson1;

import java.util.Scanner;

public class Question2 {
    public static void main(String[] args) {
        int Number1 = inputNumber1();
        int Number2 = inputNumber2();
        compareNumber(Number1, Number2);
    }

    private static void compareNumber(int Number1, int Number2) {
        if(Number1 >= Number2){
            if(Number1 == Number2) System.out.println("They equal!");
            else System.out.println("The first number is greater!");
        } else System.out.println("The second number is greater!");
    }

    private static int inputNumber1() {
        System.out.println("Input first integer number: ");
        Scanner scanner = new Scanner(System.in);
        int firstNumber = scanner.nextInt();
        return firstNumber;
    }

    private static int inputNumber2() {
        System.out.println("Input second integer number: ");
        Scanner scanner = new Scanner(System.in);
        int secondNumber = scanner.nextInt();
        return secondNumber;
    }
}
