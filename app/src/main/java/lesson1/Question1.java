package lesson1;

import java.util.Scanner;

public class Question1 {
    public static void main(String[] args) {
        int inputtedNumber = inputNumber();
        checkInputNumber(inputtedNumber);
    }

    private static int inputNumber() {
        System.out.println("Type your average score (>=0 and <=10): ");
        Scanner scanner = new Scanner(System.in);
        int inputtedNumber = scanner.nextInt();

        while (inputtedNumber <0 || inputtedNumber > 10){
            System.out.println("Score must be between 0 and 10.");
            System.out.println("Type your average score (>=0 and <=10): ");
            inputtedNumber = scanner.nextInt();
        }
        return inputtedNumber;
    }

    private static void checkInputNumber(int inputtedNumber) {
        if(inputtedNumber >= 5) System.out.println("Passed!");
            else System.out.println("Failed!");
    }

}
