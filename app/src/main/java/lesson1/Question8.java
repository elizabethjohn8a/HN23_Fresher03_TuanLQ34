package lesson1;

import java.util.Scanner;

public class Question8 {
    public static void main(String[] args) {
        int Number1 = inputNumber1();
        int Number2 = inputNumber2();
        calculateBiggestDivisor(Number1, Number2);
    }

    private static void calculateBiggestDivisor(int number1, int number2) {
        int temp1 = number1;
        int temp2 = number2;
        while (temp1 != temp2) {
            if (temp1 > temp2) {
                temp1 -= temp2;
            } else {
                temp2 -= temp1;
            }
        }
        int result = temp1;
        System.out.println("The biggest divisor of " +
                number1 + " and " + number2 + " is " + result);
    }

    private static int inputNumber1() {
        System.out.println("Input first integer number: ");
        Scanner scanner = new Scanner(System.in);
        int firstNumber = scanner.nextInt();
        return firstNumber;
    }

    private static int inputNumber2() {
        System.out.println("Input second integer number: ");
        Scanner scanner = new Scanner(System.in);
        int secondNumber = scanner.nextInt();
        return secondNumber;
    }
}
