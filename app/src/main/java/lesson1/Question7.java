package lesson1;

import java.util.Scanner;

public class Question7 {
    public static void main(String[] args) {
        int inputtedNumber = inputNumber();
        calculateEvenSum(inputtedNumber);
    }

    private static void calculateEvenSum(int inputtedNumber) {
        int sum = 0;
        for (int i = 0; i <=inputtedNumber ; i+=2) {
            sum += i;
        }
        System.out.println("Sum: " + sum);

    }

    private static int inputNumber() {
        System.out.println("Input a positive integer: ");
        Scanner scanner = new Scanner(System.in);
        int inputtedNumber = scanner.nextInt();

        while (inputtedNumber <0){
            System.out.println("Integer number must be positive.");
            System.out.println("Input a positive integer: ");
            inputtedNumber = scanner.nextInt();
        }
        return inputtedNumber;
    }
}
