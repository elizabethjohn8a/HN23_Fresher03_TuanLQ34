package lesson1;

import java.util.Scanner;

public class Question10 {
    public static void main(String[] args) {
        int inputtedNumber = inputNumber();
        check(inputtedNumber);
    }

    private static void check(int inputtedNumber) {
        if(inputtedNumber%4 == 0){
            if(inputtedNumber%100 == 0){
                if(inputtedNumber%400 == 0) System.out.println(inputtedNumber + " is a leap year!");
                else System.out.println("Not a leap year!");
            }
            if(inputtedNumber%400!=0 & inputtedNumber%100 == 0) System.out.println(inputtedNumber + " is a leap year!");
        } else if (inputtedNumber%100!=0) System.out.println("Not a leap year!");
    }

    private static int inputNumber() {
        System.out.println("Input a positive integer: ");
        Scanner scanner = new Scanner(System.in);
        int inputtedNumber = scanner.nextInt();

        while (inputtedNumber <0){
            System.out.println("Integer number must be positive.");
            System.out.println("Input a positive integer: ");
            inputtedNumber = scanner.nextInt();
        }
        return inputtedNumber;
    }
}
