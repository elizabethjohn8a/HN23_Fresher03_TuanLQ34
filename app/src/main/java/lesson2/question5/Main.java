package lesson2.question5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert number of class you wish: ");
        int insertedNumber = scanner.nextInt();
// Initialize a quarters consisting of "insertedNumber" families.
        Class[] school = new Class[insertedNumber];

        for (int i = 0; i < insertedNumber; i++) {
            System.out.println("First student: \n");
            System.out.println("Insert class number: ");
            int numberOfClass = scanner.nextInt();
            System.out.println("Insert number of student: ");
            int numberOfStudent = scanner.nextInt();
//Initialize a family consisting of "numberOfStudent" members.
            Student[] students = new Student[numberOfStudent];
            for (int j = 0; j < numberOfStudent; j++) {
                System.out.println("First student:\nInsert name:");
                String name = scanner.nextLine();
                System.out.println("Insert age: ");
                int age = scanner.nextInt();
                System.out.println("Insert address: ");
                String address = scanner.nextLine();
                System.out.println("Insert identification number: ");
                String idNumber = scanner.nextLine();
// Initialize a member
                Student student = new Student(name, age, address, idNumber);
// Save that member to family
                students[j] = student;
            }
// Save each families to quaters.
            Class aClass = new Class(numberOfStudent, numberOfClass, students);
            school[i] = aClass;


        }
        for (int i = 0; i < insertedNumber; i++) {
            System.out.println("Family " + (i+1) + ":");
            int numberOfMember = school[i].getNumberOfStudent();
            System.out.println("Number of member: " + numberOfMember);
            System.out.println("House number: " + school[i].getClassName());
            for (int j = 0; j < numberOfMember; j++) {
                Student[] students = school[i].getMembers();
                System.out.println("First member: ");
                int age = students[j].getAge();
                String address = students[j].getAddress();
                System.out.println("List of student in this class with age of 20: ");
                if(age == 20) System.out.println(students[j]);
                System.out.println("List of student in this class with age of 23 and in Ha Noi: ");
                if(address == "Ha Noi") System.out.println(students[j]);


            }



        }
    }
}
