package lesson2.question5;

/*I edited name of class KhuPho to Families. Just bcz family, not KhuPho, consists of people.
KhuPho consists of Families. */
public class Class {
    private int numberOfStudent;
    private int className;
    private Student[] members;

    public Class(int numberOfStudent, int className, Student[] members) {
        this.numberOfStudent = numberOfStudent;
        this.className = className;
        this.members = members;
    }

    public void setNumberOfStudent(int numberOfStudent) {
        this.numberOfStudent = numberOfStudent;
    }

    public void setClassName(int className) {
        this.className = className;
    }

    public void setFamily(Student[] members) {
        this.members = members;
    }

    public int getNumberOfStudent() {
        return numberOfStudent;
    }

    public int getClassName() {
        return className;
    }

    public Student[] getMembers() {
        return members;
    }
}
