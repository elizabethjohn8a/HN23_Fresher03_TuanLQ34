package lesson2.question5;

public class Student {
    private String name;
    private int age;
    private String address;
    private String identificationNumber;

    public Student(String name, int age, String address, String identificationNumber) {
        this.name = name;
        this.age = age;
        this.address = address;
        this.identificationNumber = identificationNumber;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getAddress() {
        return address;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }
}
