package lesson2.question1;

abstract class Employee {
    public String name;
    public int yearOfBirth;
    public int salary;
    public Employee(String name, int yearOfBirth, int salary) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
        this.salary = salary;
    }

    abstract int calculateSalary();

}
