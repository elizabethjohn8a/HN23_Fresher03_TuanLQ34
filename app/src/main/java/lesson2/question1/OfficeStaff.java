package lesson2.question1;

public class OfficeStaff extends Employee{
    private int numberOfWorkingDay;

    public void setNumberOfWorkingDay(int numberOfWorkingDay) {
        this.numberOfWorkingDay = numberOfWorkingDay;
    }


    public OfficeStaff(String name, int yearOfBirth, int salary, int numberOfWorkingDay) {
        super(name, yearOfBirth, salary);
        setNumberOfWorkingDay(numberOfWorkingDay);
    }

    @Override
    int calculateSalary() {
        salary = numberOfWorkingDay * 100000;
        return salary;
    }
}
