package lesson2.question1;

public class ProductionStaff extends Employee{
    private int baseSalary;
    private int numberOfProduct;
    public ProductionStaff(String name, int yearOfBirth, int salary, int baseSalary, int numberOfProduct) {
        super(name, yearOfBirth, salary);
        this.baseSalary = baseSalary;
        this.numberOfProduct = numberOfProduct;
    }

    @Override
    int calculateSalary() {
        salary = baseSalary + numberOfProduct * 5000;
        return salary;
    }
}
