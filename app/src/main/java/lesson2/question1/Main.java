package lesson2.question1;

public class Main {
    public static void main(String[] args) {
        OfficeStaff officeStaff1 = new OfficeStaff("Tuan1", 1999, 3000000, 23);
        System.out.println("Employee information: " + officeStaff1 + "\nSalary: "
                + officeStaff1.calculateSalary());
        OfficeStaff officeStaff2 = new OfficeStaff("Tuan2", 2001, 3200000, 21);
        System.out.println("Employee information: " + officeStaff2 + "\nSalary: "
                + officeStaff2.calculateSalary());

    }

}
