package lesson2.question4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert number of family you wish: ");
        int insertedNumber = scanner.nextInt();
// Initialize a quarters consisting of "insertedNumber" families.
        Families [] quarters = new Families[insertedNumber];

        for (int i = 0; i < insertedNumber; i++) {
            System.out.println("First family: \n");
            System.out.println("Insert location number of house: ");
            int numberOfHouse = scanner.nextInt();
            System.out.println("Insert number of member: ");
            int numberOfMember = scanner.nextInt();
//Initialize a family consisting of "numberOfMember" members.
            People [] members = new People[numberOfMember];
            for (int j = 0; j < numberOfMember; j++) {
                System.out.println("First member:\nInsert name:");
                String name = scanner.nextLine();
                System.out.println("Insert age: ");
                int age = scanner.nextInt();
                System.out.println("Insert business: ");
                String business = scanner.nextLine();
                System.out.println("Insert identification number: ");
                String idNumber = scanner.nextLine();
// Initialize a member
                People member = new People(name, age, business, idNumber);
// Save that member to family
                members[j] = member;
            }
// Save each families to quaters.
            Families family = new Families(numberOfMember, numberOfHouse, members);
            quarters[i] = family;


        }
        for (int i = 0; i < insertedNumber; i++) {
            System.out.println("Family " + (i+1) + ":");
            int numberOfMember = quarters[i].getNumberOfMember();
            System.out.println("Number of member: " + numberOfMember);
            System.out.println("House number: " + quarters[i].getNo());
            for (int j = 0; j < numberOfMember; j++) {
                People [] people = quarters[i].getMembers();
                System.out.println("First member: ");
                String name = people[j].getName();
                int age = people[j].getAge();
                String business = people[j].getBusiness();
                String id = people[j].getIdentificationNumber();
                System.out.println("Name: " + name + ", age: " + age
                        + ", business: " + business + ", id: " + id);
            }



        }
    }
}
