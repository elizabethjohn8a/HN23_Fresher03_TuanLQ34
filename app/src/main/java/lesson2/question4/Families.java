package lesson2.question4;

/*I edited name of class KhuPho to Families. Just bcz family, not KhuPho, consists of people.
KhuPho consists of Families. */
public class Families {
    private int numberOfMember;
    private int no;
    private People[] members;

    public Families(int numberOfMember, int no, People[] members) {
        this.numberOfMember = numberOfMember;
        this.no = no;
        this.members = members;
    }

    public void setNumberOfMember(int numberOfMember) {
        this.numberOfMember = numberOfMember;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public void setFamily(People[] members) {
        this.members = members;
    }

    public int getNumberOfMember() {
        return numberOfMember;
    }

    public int getNo() {
        return no;
    }

    public People[] getMembers() {
        return members;
    }
}
