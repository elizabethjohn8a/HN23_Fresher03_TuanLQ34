package lesson2.question4;

public class People {
    private String name;
    private int age;
    private String business;
    private String identificationNumber;

    public People(String name, int age, String business, String identificationNumber) {
        this.name = name;
        this.age = age;
        this.business = business;
        this.identificationNumber = identificationNumber;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getBusiness() {
        return business;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }
}
