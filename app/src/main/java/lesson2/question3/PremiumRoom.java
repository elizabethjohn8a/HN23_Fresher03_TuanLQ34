package lesson2.question3;

public class PremiumRoom extends Room {
    protected PremiumRoom(int duration, int priceOfService) {
        super(duration, priceOfService);
    }

    @Override
    float calculateCost() {
        float totalCost = (float) ((duration * 500000 + priceOfService)*1.05);
        this.cost = totalCost;
        return totalCost;
    }
}
