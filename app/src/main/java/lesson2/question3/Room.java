package lesson2.question3;

abstract class Room {
    protected int duration;
    protected int priceOfService;
    protected float cost;

    protected Room(int duration, int priceOfService) {
        this.duration = duration;
        this.priceOfService = priceOfService;
    }
    abstract float calculateCost();
}
