package lesson2.question3;

public class Main {
    public static void main(String[] args) {
        float deluxeRevenue = 0;
        float PremiumRevenue = 0;
        float BusinessRevenue = 0;
        DeluxeRoom deluxeRoom1 = new DeluxeRoom(2, 50000);
        deluxeRevenue += deluxeRoom1.calculateCost();
        DeluxeRoom deluxeRoom2 = new DeluxeRoom(3, 50000);
        deluxeRevenue += deluxeRoom1.calculateCost();

        PremiumRoom premiumRoom = new PremiumRoom(2, 70000);
        PremiumRevenue += premiumRoom.calculateCost();
        PremiumRoom premiumRoom1 = new PremiumRoom(3, 70000);
        PremiumRevenue += premiumRoom1.calculateCost();

        BusinessRoom businessRoom = new BusinessRoom(2);
        BusinessRevenue += businessRoom.calculateCost();
        BusinessRoom businessRoom1 = new BusinessRoom(3);
        BusinessRevenue += deluxeRoom1.calculateCost();

        System.out.println("Deluxe Revenue: " + deluxeRevenue + "\nPremium Revenue: "
                + PremiumRevenue + "\nBusiness Revenue: " + BusinessRevenue);
        float lastMonthRevenue = 3500000;
        float extraRevenueRoom;
        extraRevenueRoom = deluxeRevenue > lastMonthRevenue? deluxeRevenue:0;
        extraRevenueRoom = PremiumRevenue > lastMonthRevenue? PremiumRevenue:0;
        extraRevenueRoom = BusinessRevenue > lastMonthRevenue? BusinessRevenue:0;
        System.out.println("The highest revenue: " + extraRevenueRoom);



    }
}
