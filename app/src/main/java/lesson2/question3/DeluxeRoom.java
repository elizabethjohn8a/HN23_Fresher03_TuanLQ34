package lesson2.question3;

public class DeluxeRoom extends Room {
    protected DeluxeRoom(int duration, int priceOfService) {
        super(duration, priceOfService);
    }

    @Override
    float calculateCost() {
        float totalCost = (float) ((duration * 750000 + priceOfService)*1.05);
        this.cost = totalCost;
        return totalCost;
    }
}
