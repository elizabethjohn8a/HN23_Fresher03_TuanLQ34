package lesson2.question3;

public class BusinessRoom extends Room{
    protected BusinessRoom(int duration) {
        super(duration, 0);
    }

    @Override
    float calculateCost() {
        float totalCost = (float) ((duration * 300000 + priceOfService));
        this.cost = totalCost;
        return totalCost;
    }
}
