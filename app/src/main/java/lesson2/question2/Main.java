package lesson2.question2;

public class Main {

    public static void main(String[] args) {
        float revenue = 0;
        TypeACustomer typeACustomer = new TypeACustomer(12, 10000);
        typeACustomer.calculateTotalCost();
        revenue += typeACustomer.calculateTotalCost();
        TypeBCustomer typeBCustomer = new TypeBCustomer(12, 10000, 5);
        typeBCustomer.calculateTotalCost();
        revenue += typeBCustomer.calculateTotalCost();
        TypeBCustomer typeBCustomer2 = new TypeBCustomer(12, 10000, 11);
        typeBCustomer2.calculateTotalCost();
        revenue += typeBCustomer2.calculateTotalCost();
        TypeCCustomer typeCCustomer = new TypeCCustomer(12, 10000);
        typeCCustomer.calculateTotalCost();
        revenue += typeCCustomer.calculateTotalCost();

        System.out.println("Revenue: " + revenue);
    }
}
