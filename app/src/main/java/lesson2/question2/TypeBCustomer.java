package lesson2.question2;

public class TypeBCustomer extends Customer {
    private float discountAmount;
    public TypeBCustomer(int numberOfProduct, float price, int yearOfBonding) {
        super(numberOfProduct, price);
        float temp = (float) (yearOfBonding*0.05);
        if(temp > 0.5) this.discountAmount = 0.5F;
        else this.discountAmount = temp;
    }

    @Override
    float calculateTotalCost() {
        float sum = 0;
        sum = (float) (numberOfProduct * price * (1 - discountAmount) * 1.1);
        return sum;
    }
}
