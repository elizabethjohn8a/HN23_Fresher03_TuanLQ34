package lesson2.question2;

public class TypeCCustomer extends Customer {
    public TypeCCustomer(int numberOfProduct, float price) {
        super(numberOfProduct, price);
    }

    @Override
    float calculateTotalCost() {
        float sum = 0;
        sum = (float) (numberOfProduct * price * 0.5 * 1.1);
        return sum;
    }
}
