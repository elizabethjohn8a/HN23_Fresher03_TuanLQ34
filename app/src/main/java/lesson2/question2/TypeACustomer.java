package lesson2.question2;

public class TypeACustomer extends Customer {


    public TypeACustomer(int numberOfProduct, float price) {
        super(numberOfProduct, price);
    }

    @Override
    float calculateTotalCost() {
        float sum = 0;
        sum = (float) (numberOfProduct * price * 1.1);
        return sum;
    }
}
