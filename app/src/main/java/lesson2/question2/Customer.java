package lesson2.question2;

abstract class Customer {
    protected int numberOfProduct;
    protected float price;

    public Customer(int numberOfProduct, float price) {
        this.numberOfProduct = numberOfProduct;
        this.price = price;
    }

    abstract float calculateTotalCost();
}
