package finalExam.Exam4;

public class Main {
    public static void main(String[] args) throws contactExsistException, newPhoneNumberExistsException, contactNotFoundException {
        Management ContactManagement = new Management();
        Contact contact1 = new Contact("Tuan1", "0357555555");
        Contact contact2 = new Contact("Tuan2", "0333333333");
        Contact contact3 = contact1;
        ContactManagement.addContact(contact1);
        ContactManagement.addContact(contact2);
        //Contact Exists Exception:
        ContactManagement.addContact(contact3);

        ContactManagement.editContact("0357555555", "0111111111");
        //Contact Not Found Exception:
        ContactManagement.editContact("1234567", "1234567");
        //New Phone Exists Exception:
        ContactManagement.editContact("0357555555", "0333333333");

        //Two contact found:
        ContactManagement.searchContact("Tuan");

        ContactManagement.sortContactByName();
    }
}
