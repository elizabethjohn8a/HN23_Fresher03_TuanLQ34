package finalExam.Exam4;

import java.util.ArrayList;
import java.util.Collections;

public class Management {
    private ArrayList<Contact> listOfContacts = new ArrayList<>();

    public boolean checkPhoneNumber(String phoneNumber) {
        boolean check = false;
        for (Contact contact : listOfContacts) {
            if (contact.getPhoneNumber().equals(phoneNumber))
                check = true;
            else check = false;
        }
        return check;
    }

    public void addContact(Contact contact) throws contactExsistException {
        if (checkPhoneNumber(contact.getPhoneNumber()))
            throw new contactExsistException();
        listOfContacts.add(contact);
        System.out.println("Contact added successfully!");
    }

    public void editContact(String oldPhoneNumber, String newPhoneNumber)
            throws contactNotFoundException, newPhoneNumberExistsException {
        Contact tempContact = new Contact();
        boolean check = false;
        if (checkPhoneNumber(oldPhoneNumber)) {
            check = true;
        } else {
            throw new contactNotFoundException();
        }


        if (check) {
            for (Contact contactScript : listOfContacts) {
                if (newPhoneNumber.equals(contactScript.getPhoneNumber())) {
                    throw new newPhoneNumberExistsException();
                } else {
                    check = true;
                    tempContact = contactScript;
                }
                ;
            }
        }
        if (check) {
            tempContact.setPhoneNumber(newPhoneNumber);
        }
    }

    public Contact searchContact(String name) throws contactNotFoundException {
        Contact tempContact = new Contact();
        for (Contact contact : listOfContacts) {
            if (contact.getName().contains(name)) {
                System.out.println("Contacts found: ");
                System.out.println(contact.toString());
                tempContact = contact;
            } else {
                throw new contactNotFoundException();
            }
        }
        return tempContact;
    }

    public void sortContactByName() throws contactNotFoundException {
        ArrayList<String> nameList = new ArrayList<>();
        ArrayList<Contact> newArrayList = new ArrayList<>();
        for (Contact contact : listOfContacts) {
            nameList.add(contact.getName());
        }
        Collections.sort(nameList);
        for (String name : nameList
        ) {
            newArrayList.add(searchContact(name));
        }
        listOfContacts.clear();
        for (Contact contact : newArrayList
        ) {
            listOfContacts.add(contact);
        }
        System.out.println("Sorted list: " + listOfContacts);
    }

}
