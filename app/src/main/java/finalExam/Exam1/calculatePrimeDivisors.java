package finalExam.Exam1;

public class calculatePrimeDivisors {
    public calculatePrimeDivisors(int n){
        System.out.print("All prime divisors: ");
        int [] Prime = new int[]{2, 3, 5, 7, 11, 13, 17, 19, 23, 29};
        for (int prime: Prime) {
            if(n%prime == 0) System.out.print(prime + ", ");
        }
    }
}
