package finalExam.Exam1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class SumOfDigits {
    public int calculateSumOfDigits() throws negativeNumberException {
        System.out.println("Input a positive number: ");
        Scanner scanner = new Scanner(System.in);
        int input = 0;
        int temp = 0;
        try {
            input = scanner.nextInt();
        } catch (InputMismatchException inputMismatchException) {
            System.out.println("Wrong type!");
        }
        if (input <= 0) throw new negativeNumberException("Number must be greater than 0!");

        temp = input;
        int sum = 0;
        while (temp != 0) {
            sum += temp % 10;
            temp = temp / 10;
        }
        System.out.println("Sum: " + sum);
        return input;
    }


}
