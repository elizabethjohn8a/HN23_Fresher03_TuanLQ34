package finalExam.Exam3;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        int element = 0;
        for (int i = 1; i < 6; i++) {
            System.out.println("Enter element number " + i);
            try{
                element = scanner.nextInt();
            } catch (InputMismatchException inputMismatchException){
                inputMismatchException.printStackTrace();
            }
            arrayList.add(element);
        }
        TreeSet<Integer> treeSet = new TreeSet<>();
        System.out.println("Before sorting: " + arrayList);
        treeSet.addAll(arrayList);
        System.out.println("After sorting: " + treeSet);
        System.out.println("Enter one more element: ");
        try{
            element = scanner.nextInt();
        } catch (InputMismatchException inputMismatchException){
            inputMismatchException.printStackTrace();
        }
        treeSet.add(element);
        System.out.println("After adding number 6: " + treeSet);
    }
}
