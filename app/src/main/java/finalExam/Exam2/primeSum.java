package finalExam.Exam2;

import java.util.ArrayList;

public class primeSum {
    public primeSum() {
        ArrayList<Integer> arrayList = new ArrayList<>();
        for (int i = 10000; i < 9999999; i++) {
            int temp = i;
            while (temp != 0) {
                arrayList.add(temp % 10);
                temp = temp / 10;
            }
            int sum = 0;
            for (int number : arrayList) {
                sum += number;
            }
            int count = 0;
            for (int j = 2; j < sum; j++) {
                if (sum % j == 0) count++;
            }
            if (count == 0) System.out.println(i + ", ");
        }
    }
}
