package lesson3.question5;

public class Main {
    public static void main(String[] args){
        String aNullVariable = null;
        try {
            System.out.println(aNullVariable.length());
        } catch (NullPointerException nullPointerException){
            System.out.println("This is a null pointer exception!");
        }
    }
}
