package lesson3.question3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int arrayOfQuestion3 [] = new int[5];
        try{
            System.out.println(arrayOfQuestion3[7]);
        } catch (IndexOutOfBoundsException indexOutOfBoundsException){
            System.out.println("Exception of question 3.");
        }
        System.out.println("You've just tried to reach 7th element!");
    }
}
