package lesson3.question1;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert first float number: ");
        String firstNumber = scanner.nextLine();
        System.out.println("Insert first float number: ");
        String secondNumber = scanner.nextLine();
        float checkingVariable1 = 0;
        float checkingVariable2 = 0;
        try{
            checkingVariable1 = Float.parseFloat(firstNumber);
            System.out.print("Valid inputted number was: "+ checkingVariable1);
            checkingVariable2 = Float.parseFloat(secondNumber);
            System.out.println(", "+ checkingVariable2 + ".");
        }
        catch(NumberFormatException exception){
            System.out.println(firstNumber + " or " + secondNumber + " is not a number");
        }
        Divide(checkingVariable1, checkingVariable2);
    }

    public static void Divide(float dividend, float divisor){
        try {
            float result = dividend / divisor;
            System.out.println("The result is: " + result);
        }
        catch (ArithmeticException arithmeticException){
            System.out.println("Divisor cannot equal to 0!");
        }
    }
}
