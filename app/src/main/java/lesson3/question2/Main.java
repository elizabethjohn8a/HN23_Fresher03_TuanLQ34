package lesson3.question2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Number100FoundException {
        System.out.println("Insert number of element: ");
        Scanner scanner = new Scanner(System.in);
        int numberOfElement = scanner.nextInt();
        int arrayOfQuestion2 [] = new int[numberOfElement];
        for (int i = 0; i < arrayOfQuestion2.length; i++) {
            System.out.print("Insert element number " + (i+1) + ": ");
            int inputtedElement = scanner.nextInt();
            if (inputtedElement == 100){
                throw new Number100FoundException(arrayOfQuestion2);
            }
            arrayOfQuestion2[i] = inputtedElement;

        }
    }


}
