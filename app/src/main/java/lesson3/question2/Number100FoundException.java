package lesson3.question2;

public class Number100FoundException extends Exception{
    public Number100FoundException(int array[]) {
        System.out.println("Found element value of 100!\nArray inputted: ");
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
        System.exit(0);
    }
}
