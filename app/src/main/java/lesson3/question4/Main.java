package lesson3.question4;

public class Main {
    public static void main(String[] args) throws ParsingStringException{
        String stringExample = "15";
        System.out.println("Trying to parsing string!");
        int firstParsingTry = Integer.parseInt(stringExample);
        float secondParsingTry = Float.parseFloat(stringExample);
        throw new ParsingStringException();
    }

}
