package Lesson1;

import java.util.Scanner;

public class Question5 {
    public static void main(String[] args) {
        int inputtedNumber = inputNumber();
        calculateFactorial(inputtedNumber);
    }
    private static int inputNumber() {
        System.out.println("Input a positive integer: ");
        Scanner scanner = new Scanner(System.in);
        int inputtedNumber = scanner.nextInt();

        while (inputtedNumber <0){
            System.out.println("Integer number must be positive.");
            System.out.println("Input a positive integer: ");
            inputtedNumber = scanner.nextInt();
        }
        return inputtedNumber;
    }
    public static void calculateFactorial(int inputtedNumber){
        int result = 1;
        for (int i = 1; i <=inputtedNumber ; i++) {
            if(inputtedNumber == 0 || inputtedNumber == 1) {
                System.out.println("Result" + 1);
                break;
                }
            result *=i;
            }
        System.out.println("Factorial of " + inputtedNumber + " is " + result);
        }
}
