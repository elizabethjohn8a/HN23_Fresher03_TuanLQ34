package Lesson1;

import java.util.Scanner;

public class Question9 {
    public static void main(String[] args) {
        int inputtedNumber = inputNumber();
        calculatePrime(inputtedNumber);
    }

    private static void calculatePrime(int inputtedNumber) {
        if (inputtedNumber <= 1) {
            System.out.println("Not a prime!");
            for (int i = 2; i <= inputtedNumber / 2; i++) {
                if (inputtedNumber % i == 0) {
                    System.out.println("Not a prime!");
                    ;
                }
            }
        } else System.out.println(inputtedNumber + " is a prime!");
    }

    private static int inputNumber() {
        System.out.println("Input a positive integer: ");
        Scanner scanner = new Scanner(System.in);
        int inputtedNumber = scanner.nextInt();

        while (inputtedNumber <0){
            System.out.println("Integer number must be positive.");
            System.out.println("Input a positive integer: ");
            inputtedNumber = scanner.nextInt();
        }
        return inputtedNumber;
    }
}
