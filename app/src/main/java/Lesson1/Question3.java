package Lesson1;

import java.util.Scanner;

public class Question3 {
    public static void main(String[] args) {
        int Number1 = inputNumber1();
        int Number2 = inputNumber2();
        dashBoard(Number1, Number2);

    }

    private static void dashBoard(int number1, int number2) {
        System.out.println("Select an operator: \n1. Plus.\n2.Minus\n3.Time\n4.Divide.");
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();
        switch (input){
            case 1:
                System.out.println(number1 + "+" + number2 + "=" + (number1 + number2));
            case 2:
                System.out.println(number1 + "-" + number2 + "=" + (number1 - number2));
            case 3:
                System.out.println(number1 + "*" + number2 + "=" + (number1 * number2));
            case 4:
                float number11 = number1;
                float number22 = number2;
                System.out.println(number11 + "/" + number22 + "=" + (number11 / number22));
            default:
                return;
        }
    }

    private static int inputNumber1() {
        System.out.println("Input first integer number: ");
        Scanner scanner = new Scanner(System.in);
        int firstNumber = scanner.nextInt();
        return firstNumber;
    }

    private static int inputNumber2() {
        System.out.println("Input second integer number: ");
        Scanner scanner = new Scanner(System.in);
        int secondNumber = scanner.nextInt();
        return secondNumber;
    }
}
