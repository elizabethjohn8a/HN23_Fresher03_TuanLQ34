package Lesson1;

import java.util.Scanner;

public class Question6 {
    public static void main(String[] args) {
        int inputtedNumber = inputNumber();
        calculateSum(inputtedNumber);
    }

    private static void calculateSum(int inputtedNumber) {
        int sum = 0;
        for (int i = 1; i <inputtedNumber ; i++) {
            sum += i;
        }
        System.out.println("Sum: " + sum);

    }

    private static int inputNumber() {
        System.out.println("Input a positive integer: ");
        Scanner scanner = new Scanner(System.in);
        int inputtedNumber = scanner.nextInt();

        while (inputtedNumber <0){
            System.out.println("Integer number must be positive.");
            System.out.println("Input a positive integer: ");
            inputtedNumber = scanner.nextInt();
        }
        return inputtedNumber;
    }
}
