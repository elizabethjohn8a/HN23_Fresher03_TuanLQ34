package lesson6.question1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;

public class Main {
    public static void main(String[] args) throws IOException {
        HashSet<String> hashSet = new HashSet<>();
        FileReader fileReader = new FileReader("data.txt");
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = bufferedReader.readLine();

        while (line != null) {
            for (String word : line.split(" ")) {
                hashSet.add(word);
            }
            line = bufferedReader.readLine();
        }
        System.out.println("Number of unique word: " + hashSet.size());
        System.out.println("List of unique words: " + hashSet);
        fileReader.close();
        bufferedReader.close();

    }
}
