package lesson6.question4;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class WriteToFile {


    public WriteToFile() throws IOException {
        FileWriter fileWriter = new FileWriter("nhanvien.txt", true);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        NhanVien[] nhanVien = input();
        for (int i = 0; i < nhanVien.length; i++) {
            bufferedWriter.write(nhanVien[i].toString());
        }
        System.out.println("Saved!");
        bufferedWriter.close();
        fileWriter.close();

    }

    public NhanVien[] input(){
        NhanVien nhanVien1 = inputEmployee();
        NhanVien nhanVien2 = inputEmployee();
        NhanVien nhanVien3 = inputEmployee();
        return new NhanVien[]{nhanVien1, nhanVien2, nhanVien3};
    }

    public NhanVien inputEmployee(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ma NV: ");
        String maNV = scanner.nextLine();
        System.out.println("Ho ten: ");
        String hoTen = scanner.nextLine();
        System.out.println("Tuoi: ");
        int tuoi = scanner.nextInt();
        System.out.println("Luong: ");
        float luong = scanner.nextFloat();
        return new NhanVien(maNV, hoTen, tuoi, luong);

    }
}
