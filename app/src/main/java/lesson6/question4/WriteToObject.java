package lesson6.question4;

import java.io.FileOutputStream;
import java.io.IOException;

public class WriteToObject {
    public WriteToObject() throws IOException {
        WriteToFile writeToFile = new WriteToFile();
        NhanVien[]nhanVien = writeToFile.input();
        FileOutputStream fileOutputStream = new FileOutputStream("nhanvien.bin");
        java.io.ObjectOutputStream objectOutputStream = new java.io.ObjectOutputStream(fileOutputStream);
        for (int i = 0; i < nhanVien.length; i++) {
            objectOutputStream.writeObject(nhanVien[i]);
        }
        System.out.println("Saved!");
        fileOutputStream.close();
        objectOutputStream.close();
    }
}
