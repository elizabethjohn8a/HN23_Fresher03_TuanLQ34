package lesson6.question4;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class ReadFromObject {
    public ReadFromObject() throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("nhanvien.bin");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        NhanVien nhanVien = (NhanVien) objectInputStream.readObject();
        while (nhanVien!=null){
            System.out.println(nhanVien);
            nhanVien = (NhanVien) objectInputStream.readObject();
        }

    }
}
