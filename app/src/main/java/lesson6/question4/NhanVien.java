package lesson6.question4;

public class NhanVien {
    String maNV;
    String hoTen;
    int tuoi;
    float luong;

    public String getMaNV() {
        return maNV;
    }

    public void setMaNV(String maNV) {
        this.maNV = maNV;
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public int getTuoi() {
        return tuoi;
    }

    public void setTuoi(int tuoi) {
        this.tuoi = tuoi;
    }

    public float getLuong() {
        return luong;
    }

    public void setLuong(float luong) {
        this.luong = luong;
    }

    public NhanVien(String maNV, String hoTen, int tuoi, float luong) {
        this.maNV = maNV;
        this.hoTen = hoTen;
        this.tuoi = tuoi;
        this.luong = luong;
    }


    @Override
    public String toString() {
        return maNV + " " + hoTen + " " + tuoi + " " + luong + "\n";
    }
}
