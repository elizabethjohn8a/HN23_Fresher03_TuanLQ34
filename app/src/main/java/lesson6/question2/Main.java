package lesson6.question2;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Custom custom = insert();
        FileWriter fileWriter = new FileWriter("customData.txt", true);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        bufferedWriter.write(custom.toString());
        System.out.println("Saved!");
        bufferedWriter.flush();
        bufferedWriter.close();
        fileWriter.close();
    }
    static Custom insert(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter id:");
        String id = scanner.nextLine();
        System.out.println("Enter name: ");
        String name = scanner.nextLine();
        System.out.println("Enter phone number: ");
        String phoneNumber = scanner.nextLine();
        return new Custom(id, name, phoneNumber);
    }

}
class Custom {
    private String id;
    private String name;
    private String phoneNumber;

    public Custom(String id, String name, String phoneNumber) {
        this.id = id;
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return getId() + " " + getName() + " " + getPhoneNumber() + "\n";
    }
}