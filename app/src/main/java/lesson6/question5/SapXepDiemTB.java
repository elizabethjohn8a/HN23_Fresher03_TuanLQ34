package lesson6.question5;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.TreeSet;

public class SapXepDiemTB {
    public SapXepDiemTB() throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("sinhvien.txt");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        TreeSet<Float> nameTreeSet = new TreeSet<>();
        SinhVien sinhVien = (SinhVien) objectInputStream.readObject();
        float diemTB = sinhVien.getDiemTB();
        while (sinhVien!=null){
            nameTreeSet.add(diemTB);
            try{
                System.out.println(sinhVien.getDiemTB());
                sinhVien = (SinhVien) objectInputStream.readObject();
                diemTB = sinhVien.getDiemTB();
            } catch (NullPointerException nullPointerException){
                System.out.println("Ket thuc danh sach");
            }
        }
        System.out.println("Diem trung binh lan luot la: " + nameTreeSet);
        objectInputStream.close();
    }
}

