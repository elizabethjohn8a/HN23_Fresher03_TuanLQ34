package lesson6.question5;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.TreeSet;

public class SapXepHoTen {
    public SapXepHoTen() throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("sinhvien.txt");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        TreeSet<String> nameTreeSet = new TreeSet<>();
        SinhVien sinhVien = (SinhVien) objectInputStream.readObject();
        String name = sinhVien.getHoTen();
        while (sinhVien!=null){
            nameTreeSet.add(name);
            try{
                sinhVien = (SinhVien) objectInputStream.readObject();
                name = sinhVien.getHoTen();
            } catch (NullPointerException nullPointerException){
                System.out.println("Ket thuc danh sach");
            }
        }
        System.out.println(nameTreeSet);
        objectInputStream.close();
    }
}
