package lesson6.question5;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class HienThi {
    public HienThi() throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("sinhvien.txt");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        SinhVien sinhVien = (SinhVien) objectInputStream.readObject();
        while (objectInputStream!=null){
            System.out.println(sinhVien);
            sinhVien = (SinhVien) objectInputStream.readObject();
        }
        objectInputStream.close();
    }
}
