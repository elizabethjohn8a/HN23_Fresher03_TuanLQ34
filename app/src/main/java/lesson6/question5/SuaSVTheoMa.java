package lesson6.question5;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.InputMismatchException;
import java.util.Scanner;

public class SuaSVTheoMa {
    public SuaSVTheoMa() throws IOException, ClassNotFoundException {
        System.out.println("Nhap ma sinh vien: ");
        Scanner scanner = new Scanner(System.in);
        String maSV = null;
        try{
            maSV =  scanner.nextLine();
        } catch (InputMismatchException inputMismatchException){
            System.out.println("Nhap sai dinh dang!");
        }
        FileInputStream fileInputStream = new FileInputStream("sinhvien.txt");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        SinhVien sinhVien = (SinhVien) objectInputStream.readObject();
        while (sinhVien!=null){
            String maSinhVien = sinhVien.getMaSV();
            if(maSinhVien.equals(maSV)){
                ThemSinhVien themSinhVien = new ThemSinhVien();
                System.out.println("Saved!");
                break;
            }
            try{
                sinhVien = (SinhVien) objectInputStream.readObject();}
            catch (NullPointerException nullPointerException){
                System.out.println("Khong tim thay sinh vien!");
            }
            }
        }
    }

