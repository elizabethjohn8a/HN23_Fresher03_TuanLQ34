package lesson6.question5;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class ThemSinhVien {
    public ThemSinhVien() throws IOException {
        SinhVien sinhVien = new Input().InputStudent();
        FileOutputStream fileOutputStream = new FileOutputStream("sinhvien.txt");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(sinhVien);
    }
}
