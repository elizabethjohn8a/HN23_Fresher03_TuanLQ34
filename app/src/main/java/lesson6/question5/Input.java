package lesson6.question5;

import java.util.Scanner;

public class Input {
    public SinhVien InputStudent(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ma SV: ");
        String maSV = scanner.nextLine();
        System.out.println("Ho ten: ");
        String hoTen = scanner.nextLine();
        System.out.println("Dia chi: ");
        String diaChi = scanner.nextLine();
        System.out.println("Tuoi: ");
        int tuoi = scanner.nextInt();
        System.out.println("Diem TB: ");
        float diemTB = scanner.nextFloat();
        return new SinhVien(maSV, hoTen, tuoi, diaChi, diemTB);
    }
}
