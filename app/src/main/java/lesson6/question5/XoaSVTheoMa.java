package lesson6.question5;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class XoaSVTheoMa {
    public XoaSVTheoMa() throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("sinhvien.txt");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        Object object = objectInputStream.readObject();
        SinhVien [] sinhViens = new SinhVien[10];
        for (int i = 0; i < sinhViens.length; i++) {
            while (object!=null){
                sinhViens[i] = (SinhVien) object;
                object = objectInputStream.readObject();
            }
        }
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("sinhvien.txt"));
        for(SinhVien sinhVien: sinhViens){
            objectOutputStream.writeObject(sinhVien);
        }
        objectOutputStream.flush();
        objectOutputStream.close();
        objectInputStream.close();




    }
}
