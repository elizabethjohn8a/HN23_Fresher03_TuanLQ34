package lesson6.question3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        String id = insert();
        search(id);
    }
    static String insert(){
        System.out.println("Insert id to get average point: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
    static void search(String id) throws IOException {
        FileReader fileReader = new FileReader("sinhvien.txt");
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = readLineFromText(bufferedReader);
        String []tempLine = splitLine(line);
        while (line != null){
            if(id.equals(tempLine[0]) ){
                System.out.println("Your score: " + tempLine[2]);
                break;
            }
            line = readLineFromText(bufferedReader);
            try{
                tempLine = splitLine(line);
            } catch (NullPointerException nullPointerException){
                System.out.println("No student matched!");
            }

        }
        fileReader.close();
        bufferedReader.close();

    }
    static String readLineFromText(BufferedReader bufferedReader) throws IOException {
        return bufferedReader.readLine();
    }
    static String [] splitLine(String line){
        return line.split(" ");
    }


}
