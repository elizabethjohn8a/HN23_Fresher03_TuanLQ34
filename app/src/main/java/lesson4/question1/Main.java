package lesson4.question1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        String colors[] = new String []{"red", "orange", "yellow", "green", "blue"};
        String colors2[] = new String []{"violet", "purple", "black", "white", "brown"};
        List<String> list = new ArrayList<>(Arrays.asList(colors));
        List<String> list2 = new ArrayList<>(Arrays.asList(colors2));
        list.addAll(list2);
        list2.clear();
        System.out.println("List after merging: " + list);
        for (int i = 0; i < list.size(); i++) {
            list.set(i, list.get(i).toUpperCase());
        }
        System.out.println("Upper case list: " + list);

        for (int i = 1; i <=3 ; i++) {
            list.remove(3);
        }
        System.out.println("List after deleting 3 elements: " + list);

        Collections.reverse(list);
        System.out.println("Reverse list: " + list);
    }
}
