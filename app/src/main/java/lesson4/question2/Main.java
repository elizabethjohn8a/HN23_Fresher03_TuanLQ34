package lesson4.question2;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        HashMap<Integer, Student> studentHashMap = new HashMap<>();
        System.out.println("1. Input number of student. \n2.Show all students.\n" +
                "3. Search student by rollNo ");
        int choice = scanner.nextInt();
        switch (choice){
            case 1:
                System.out.println("Please input number of student.");
                int numberOfStudent = scanner.nextInt();
                for (int i = 0; i < numberOfStudent; i++) {
                    int rollNo = studentHashMap.isEmpty()?1:studentHashMap.size();
                    System.out.println("Input name: ");
                    String name = scanner.nextLine();
                    System.out.println("Input sex: ");
                    String sex = scanner.nextLine();
                    System.out.println("Input age: ");
                    int age = scanner.nextInt();
                    System.out.println("Input email: ");
                    String email = scanner.nextLine();
                    System.out.println("Input address: ");
                    String address = scanner.nextLine();
                    Student student = new Student(rollNo, name, sex, age, email, address);
                    studentHashMap.put(rollNo, student);
                }
            case 2:
                Iterator<Map.Entry<Integer, Student>> iterator = studentHashMap.entrySet().iterator();
                while (iterator.hasNext()){
                    Map.Entry<Integer, Student> next = iterator.next();
                    System.out.println(next.getKey() + ". " + next.getValue());
                }
            case 3:
                Iterator<Map.Entry<Integer, Student>> iterator2 = studentHashMap.entrySet().iterator();
                System.out.println("Input student roll no: ");
                int rollNo = scanner.nextInt();
                int count = 0;
                while (iterator2.hasNext()){
                    Map.Entry<Integer, Student> next = iterator2.next();
                    if (next.getKey() == rollNo){
                    System.out.println("Here is the result: " + next.getValue() + "\n");
                    count ++;
                    }
                }
                if (count == 0) System.out.println("No matching student found!");
        }
    }
}
