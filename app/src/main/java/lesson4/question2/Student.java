package lesson4.question2;

public class Student {
    private int rollNo;
    private String name;
    private String sex;
    private int age;
    private String email;
    private String address;

    public Student(int rollNo, String name, String sex, int age, String email, String address) {
        this.rollNo = rollNo;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.email = email;
        this.address = address;
    }
}
