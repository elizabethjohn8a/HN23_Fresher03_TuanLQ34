package lesson4.question3;

public class Employee {
    private static int id = 0;
    private String name;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setProfessionalLevel(String professionalLevel) {
        this.professionalLevel = professionalLevel;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setNumberOfChild(int numberOfChild) {
        this.numberOfChild = numberOfChild;
    }

    private String sex;
    private String dateOfBirth;
    private String phoneNumber;
    private String professionalLevel;
    private String address;
    private int numberOfChild;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSex() {
        return sex;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getProfessionalLevel() {
        return professionalLevel;
    }

    public String getAddress() {
        return address;
    }

    public int getNumberOfChild() {
        return numberOfChild;
    }

    public Employee(String name, String sex, String dateOfBirth, String phoneNumber, String address, int numberOfChild, String professionalLevel) {
        this.name = name;
        this.sex = sex;
        this.dateOfBirth = dateOfBirth;
        this.phoneNumber = phoneNumber;
        this.address = address=="0"?null:address;
        this.numberOfChild = numberOfChild==0?null:numberOfChild;
        this.professionalLevel = professionalLevel=="0"?null:professionalLevel;
        this.id++;
    }
}
