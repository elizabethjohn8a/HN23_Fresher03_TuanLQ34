package lesson4.question3;

import android.annotation.SuppressLint;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class Main {
    @SuppressLint("NewApi")
    public static void main(String[] args) {
        HashMap<Integer, Employee> employeeHashMap = new HashMap<>();
        System.out.println("1. Type \"add\" to add 1 more employee: " +
                "2. Type \"display\" to show employee information: " +
                "3. Type \"update\" to update employee information: " +
                "4. Type \"find\" to find employee by id or name: ");
        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();
        switch(choice){
            case 1:
                System.out.println("Insert name (compulsory): ");
                String name = scanner.nextLine();
                System.out.println("Insert sex (compulsory), Male/Female: ");
                String sex = scanner.nextLine();
                while (sex!= "Male" || sex!= "Female"){
                    System.out.println("Wrong input. Insert sex again (compulsory), Male/Female: ");
                    sex = scanner.nextLine();
                }
                System.out.println("Insert date of birth (compulsory), dd/mm/yyyy: ");
                String dateOfBirth = scanner.nextLine();
                while (dateOfBirth.length()!=10){
                    System.out.println("Wrong syntax. Insert date of birth (compulsory), dd/mm/yyyy: ");
                    dateOfBirth = scanner.nextLine();
                }
                System.out.println("Insert phone number (compulsory): ");
                String phoneNumber = scanner.nextLine();
                System.out.println("Insert professional level " +
                        "(optional: intermediate, college, university or 0 to skip): ");
                String professionalLevel = scanner.nextLine();
                while (professionalLevel!= "intermediate" & professionalLevel!="college"
                        & professionalLevel!="university" & professionalLevel!="0"){
                    System.out.println("Wrong insert. Insert professional level  +" +
                            "                        (optional: intermediate, college, university or 0 to skip): ");
                    professionalLevel = scanner.nextLine();
                }
                System.out.println("Insert address (optional, type 0 to skip): ");
                String address = scanner.nextLine();
                System.out.println("Insert number of child (optional, type 0 to skip): ");
                int numberOfChild = scanner.nextInt();
//Initialize an employee, then put him in employeeHashMap
                Employee employee = new Employee(name, sex, dateOfBirth, phoneNumber, address,
                        numberOfChild, professionalLevel);
                employeeHashMap.put(employee.getId(), employee);
            case 2:
                if(employeeHashMap.isEmpty()) {
// If HashMap is empty, print:
                    System.out.println("Not found information! Please insert at step 1.");
                    System.exit(0);
                    }
                Iterator<Map.Entry<Integer, Employee>> iterator = employeeHashMap.entrySet().iterator();
                while (iterator.hasNext()){
                    Map.Entry<Integer, Employee> next = iterator.next();
                    System.out.println(next.getKey() + ". " + next.getValue());
                }
            case 3:
                Iterator<Map.Entry<Integer, Employee>> iterator2 = employeeHashMap.entrySet().iterator();
                System.out.println("Input employee id to update: ");
                int id = scanner.nextInt();
                while (iterator2.hasNext()){
                    Map.Entry<Integer, Employee> next = iterator2.next();
//Search for employee with given id
                    if (next.getKey() == id){
//If caught, update their information
                        Employee tempEmployee = next.getValue();
                        System.out.println("Insert name (compulsory): ");
                        String name1 = scanner.nextLine();
                        tempEmployee.setName(name1);
                        System.out.println("Insert sex (compulsory), Male/Female: ");
                        String sex1 = scanner.nextLine();
                        while (sex1!= "Male" || sex1!= "Female"){
                            System.out.println("Wrong input. Insert sex again (compulsory), Male/Female: ");
                            sex1 = scanner.nextLine();
                            }

                            System.out.println("Insert date of birth (compulsory), dd/mm/yyyy: ");
                            String dateOfBirth1 = scanner.nextLine();
                            while (dateOfBirth1.length()!=10){
                                System.out.println("Wrong syntax. Insert date of birth (compulsory), dd/mm/yyyy: ");
                                dateOfBirth1 = scanner.nextLine();
                            }
                            tempEmployee.setDateOfBirth(dateOfBirth1);

                            System.out.println("Insert phone number (compulsory): ");
                            String phoneNumber1 = scanner.nextLine();
                            tempEmployee.setPhoneNumber(phoneNumber1);

                    System.out.println("Update successfully");
                    }
                }
            case 4:
                System.out.println("1. Find by id \n2. Find by id");
                int choice1 = scanner.nextInt();
                String temp = Integer.toString(choice1);
// Check if user didn't type anything
                while (temp.isEmpty()){
                    System.out.println("1. Find by id \n2. Find by id");
                    temp = String.valueOf(scanner.nextInt());
                }
                choice1 = Integer.parseInt(temp);
                Iterator<Map.Entry<Integer, Employee>> iterator3 = employeeHashMap.entrySet().iterator();
                Iterator<Map.Entry<Integer, Employee>> iterator4 = employeeHashMap.entrySet().iterator();

                int resultCount = 0;
                switch (choice1){
                    case 1:
                        System.out.println("Input employee id to find: ");
                        int inputtedId = scanner.nextInt();
                        while (iterator3.hasNext()){
                            Map.Entry<Integer, Employee> next = iterator3.next();
                            if(next.getKey() == inputtedId) {
                                System.out.println("Searching result: " + next.getValue());
                                resultCount++;
                            }
                        }
//resultCount variable is used to check whether a user is found or not.
                        if(resultCount == 0) System.out.println("Not Found Information!");
                    case 2:
                        resultCount = 0;
                        System.out.println("Input employee name to find: ");
                        String inputtedName = scanner.nextLine();
                        while (iterator4.hasNext()){
                            Map.Entry<Integer, Employee> next = iterator4.next();
                            if(next.getValue().getName() == inputtedName) System.out.println("Searching result: " + next.getValue());
                            resultCount++;
                        }
                        if(resultCount == 0) System.out.println("Not Found Information!");

                }
            default:
                System.out.println("Wrong choice. System will exit automatically.");
                System.exit(0);
                }

        }
    }
