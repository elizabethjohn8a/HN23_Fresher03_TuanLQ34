package lesson5.question4;

import java.util.ArrayList;
import java.util.Random;

public class Test {
    public static void main(String[] args) throws InterruptedException {
        Thread1 thread1 = new Thread1();
        Thread2 thread2 = new Thread2();

        thread1.start();

        thread1.join();


        thread2.start();

        thread2.join();

        System.out.println("List 1: " + thread1.getList1());
        System.out.println("List 2: " + thread2.getList2());

    }
}

class Thread1 extends Thread {
    ArrayList<Integer> list1 = new ArrayList<>();

    public ArrayList<Integer> getList1() {
        return list1;
    }

    @Override
    public void run() {
        int random_int;
        for (int i = 0; i < 10; i++) {
            random_int = (int) Math.floor(Math.random() * (100) + 1 + -100);
            list1.add(random_int);
            System.out.println("Random number " + (i + 1) + " created: " + random_int);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}


class Thread2 extends Thread {
    public ArrayList<Character> getList2() {
        return list2;
    }

    ArrayList<Character> list2 = new ArrayList<>();

    @Override
    public void run() {
        char c;
        for (int i = 0; i < 10; i++) {
            Random rnd = new Random();
            c = (char) ('a' + rnd.nextInt(26));
            list2.add(c);
            System.out.println("Character number " + (i + 1) + " created: " + c);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}