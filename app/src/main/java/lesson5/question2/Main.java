package lesson5.question2;

import java.util.ArrayList;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        CharacterTesting characterTesting = new CharacterTesting();

        Thread thread1 = new Thread() {
            @Override
            public void run() {
                while (true) {
                    characterTesting.randomCharacterGenerate();
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    characterTesting.totalTimeRunning += 2;
                    if (characterTesting.totalTimeRunning > 20) {
                        break;
                    }

                }
            }
        };
        thread1.start();

        Thread thread2 = new Thread() {
            @Override
            public void run() {
                while (true) {
                    characterTesting.displayUpperCase();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    characterTesting.totalTimeRunning += 1;
                    if (characterTesting.totalTimeRunning > 20) {
                        System.out.println("Out of duration!");
                        thread1.interrupt();
                        Thread.currentThread().interrupt();
                        break;
                    }

                }
                Thread.currentThread().interrupt();
            }
        };
        thread2.start();

    }

}

class CharacterTesting {
    private ArrayList<Character> listCharacter = new ArrayList<>();
    int totalTimeRunning = 0;

    public synchronized void randomCharacterGenerate() {
        Random rnd = new Random();
        char c = (char) ('a' + rnd.nextInt(26));
        listCharacter.add(c);
        notify();
    }

    public synchronized void displayUpperCase() {
        for (int i = 0; i < listCharacter.size(); i++) {
            System.out.print((char) (listCharacter.get(i) - 32) + ", ");
            try {
                wait();
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }

        }

    }
}
