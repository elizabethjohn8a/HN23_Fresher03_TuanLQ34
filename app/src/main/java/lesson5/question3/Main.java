package lesson5.question3;

public class Main {
    public static void main(String[] args) {
        Data data = new Data();
        Thread thread1 = new Thread() {
            @Override
            public void run() {
                while (true) {
                    data.randomNumberGenerate1();
                    System.out.println("Current total: " + data.getTotal());
                    if (data.getTotal() <= -100 || data.getTotal() >= 100) {
                        Thread.currentThread().interrupt();
                        break;
                    }
                }
            }
        };
        thread1.start();

        Thread thread2 = new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {
                        data.randomNumberGenerate2();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    System.out.println("Current total: " + data.getTotal());
                    if (data.getTotal() <= -100 || data.getTotal() >= 100) {
                        thread1.interrupt();
                        Thread.currentThread().interrupt();
                        break;
                    }

                }
            }
        };
        thread2.start();


    }
}

class Data {
    private int total = 0;

    public synchronized void randomNumberGenerate1() {
        int random_int = (int) Math.floor(Math.random() * (100) + 1 + -100);
        total += random_int;
        notify();
    }

    public synchronized void randomNumberGenerate2() throws InterruptedException {
        int random_int = (int) Math.floor(Math.random() * (100) + 1);
        total += random_int;
        wait();
    }

    public int getTotal() {
        return total;
    }
}

