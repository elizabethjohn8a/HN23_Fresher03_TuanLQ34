package lesson5.question1;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Number number = new Number();
        Thread thread1 = new Thread() {
            @Override
            public void run() {
                while (true) {
                    number.randomNumberGenerate();
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        };
        thread1.start();

        Thread thread2 = new Thread() {
            @Override
            public void run() {
                while (true) {
                    number.displaySquare();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        };
        thread2.start();

    }
}

class Number {
    private ArrayList<Integer> listNumber = new ArrayList<>();

    public synchronized void randomNumberGenerate() {
        int random_int = (int) Math.floor(Math.random() * 20 + 1);
        listNumber.add(random_int);
        notify();
    }

    public synchronized void displaySquare() {
        for (int i = 0; i < listNumber.size(); i++) {
            System.out.print(listNumber.get(i) * listNumber.get(i) + ", ");
            try {
                wait();
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }

        }

    }
}

