package lesson5.question5;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        String[] province = new String[]{"Ha Noi", "Hung Yen", "Thanh Hoa", "Ha Tinh", "Quang Ninh"};
        String[] nameList = new String[]{"Nguyen Van Huan", "Nguyen Linh Duc", "Nguyen Van Tuan",
                "Vu Viet Tung", "Tran Trung Nghia"};
        Thread thread1 = new Thread() {
            @Override
            public synchronized void run() {
                for (int i = 0; i < nameList.length; i++) {
                    System.out.print("Name: " + nameList[i] + ", ");
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        };

        Thread thread2 = new Thread() {
            @Override
            public synchronized void run() {
                for (int i = 0; i < province.length; i++) {
                    System.out.println("province: " + province[i] + "...");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    notify();
                }

            }
        };

        thread1.start();

        thread2.start();
    }


}


